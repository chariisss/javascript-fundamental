// let biodata = {
//     namaLengkap: "Charisma Kurniawan Aji",
//     namaPanggilan: "Charis",
//     umur: 23,
//     alamat: "BSD",
// }

// console.log(biodata);

function dataMahasiswa(nama, umur, alamat, makananFavorit, minumanFavorit) {
    this.name = nama;
    this.age = umur;
    this.adress = alamat;
    this.favoriteFood = {
        favoriteFood: makananFavorit,
        favoriteDrink: minumanFavorit
    }
}

let mahasiswa1 = new dataMahasiswa("charis", 23, "BSD", "chicken khungpao", "thaitea");
let mahasiswa2 = new dataMahasiswa("kurniawan", 23, "Serpong", "mie ayam", "coklat");
let mahasiswa3 = new dataMahasiswa("aji", 23, "cisauk", "meat", "air putih");

let totalMahasiswa = []
totalMahasiswa.push(mahasiswa1, mahasiswa2, mahasiswa3)
console.log(totalMahasiswa, totalMahasiswa.length)
