// Cara 1 promise

// const getFirstName = () => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai("Charisma") //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getMiddleName = (firstName) => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`${firstName} Kurniawan`) //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getLastName = (secondName) => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`${secondName} Aji`) //selesai diganti resolve
//         }, 2000)
//     })
// }


// const getName = () => {
//     getFirstName()
//         .then((result) => {
//             return getMiddleName(result)
//         })
//         .then((result2) => {
//             return getLastName(result2)
//         })
//         .then((result3) => {
//             console.log(`Nama saya ${result3}`)
//         })
// }

// getName();

// const getFirstName = () => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai("Charisma") //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getMiddleName = (firstName) => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`Kurniawan`) //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getLastName = (secondName) => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`Aji`) //selesai diganti resolve
//         }, 2000)
//     })
// }

// cara 2 

// const getName = () => {
//     Promise.all([getFirstName(), getMiddleName(), getLastName()])
//     .then(([result,result2,result3]) => {
//         const firstName = result
//         const middleName = result2
//         const lastName = result3
//         const name = `Nama saya ${firstName} ${middleName} ${lastName}`

//         console.log(name)
//     })
// }

// getName ()

// cara 3

// const getFirstName = () => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai("Charisma") //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getMiddleName = () => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`Kurniawan`) //selesai diganti resolve
//         }, 2000)
//     })
// }
// const getLastName = () => {
//     return new Promise((selesai) => {
//         setTimeout(() => {
//             selesai(`Aji`) //selesai diganti resolve
//         }, 2000)
//     })
// }

// const getName = async () => {
//     const firstName = await getFirstName ()
//     const middleName = await getMiddleName ()
//     const lastName = await getLastName ()

//     const name = `Nama saya ${firstName} ${middleName} ${lastName}`
//     console.log(name)
// }

// getName ()


const getNama = () => {
        return new Promise((selesai) => {
            setTimeout(() => {
                selesai("Charisma Kurniawan Aji") //selesai diganti resolve
            }, 2000)
        })
    }
const getAlamat = () => {
    return new Promise((selesai) => {
        setTimeout(() => {
            selesai("BSD") //selesai diganti resolve
        }, 2000)
    })
}
const getHobi = () => {
    return new Promise((selesai) => {
        setTimeout(() => {
            selesai("Traveling") //selesai diganti resolve
        }, 2000)
    })
}

const getUmur = () => {
    return new Promise((selesai) => {
        setTimeout(() => {
            selesai("23") // selesai diganti resolve
        })
    })
}

const getData = async () => {
    const nama = await getNama ()
    const alamat = await getAlamat ()
    const hobi = await getHobi ()
    const umur = await getUmur ()

    const data = {
        nama : nama,
        alamat : alamat,
        hobi : hobi,
        umur : umur
    }
    console.log(data)
}

getData ()