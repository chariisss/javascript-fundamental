// const getFirstName = () => {
//     return 'Akmal'
// }

// const getLastName = () => {
//     return 'Ghaffari'
// }

// const getName = () => {
//     const firstName = getFirstName()
//     const lastName = getLastName()
//     const name = firstName + lastName
//     console.log(name)
// }

                                                    //Menggunakan Callback
// const getFirstName = (callback) => {
//     setTimeout(()=>{
//         callback('Akmal')
//     }, 3000)
// }

// const getLastName = () => {
//     return 'Ghaffari'
// }

// const getName = () => {
//     getFirstName((result)=>{
//         const firstName = result
//         const lastName = getLastName()
//         const name = firstName + lastName
//         console.log(name)
//     }) 
// }

                                                    //Menggunakan 2 Callback
// const getFirstName = (callback) => {
//     setTimeout(()=>{
//         callback('Akmal')
//     }, 3000)
// }

// const getLastName = (cb) => {
//     setTimeout(()=>{
//         cb('Ghaffari')
//     }, 3000)
// }

// const getName = () => {
//     getFirstName((result1)=>{
//         getLastName((result2)=>{
//             const firstName = result1
//             const lastName = result2
//             const name = firstName + lastName
//             console.log(name)
//         })
//     })
// }

                                                // Menggunakan Promise cara 1
// const getFirstName = () => {
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve('Akmal')
//         }, 3000)
//     })
// }

// const getLastName = (firstName) => {
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve(firstName + ' Ghaffari')
//         }, 3000)
//     })
// }

// const getThirdName = (lastName) => {
//     return new Promise((resolve)=>{
//         setTimeout(()=>{
//             resolve(lastName + ' Widodo')
//         }, 3000)
//     })
// }

// const getName = () => {
//     getFirstName()
//     .then((result)=>{
//         return getLastName(result)
//     })
//     .then((result2)=>{
//         return getThirdName(result2)
//     })
//     .then((nama)=>{
//         console.log(nama)
//     })
// }

                                                // Menggunakan Promise cara 2
// const getFirstName = () => {
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve('Akmal')
//         }, 3000)
//     })
// }

// const getLastName = () => {
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve('Ghaffari')
//         }, 3000)
//     })
// }

// const getThirdName = () => {
//     return new Promise((resolve, reject)=>{
//         setTimeout(()=>{
//             resolve('Widodo')
//         }, 3000)
//     })
// }

// const getName = () => {
//     Promise.all([getFirstName(), getLastName(), getThirdName()])
//     .then(([result1, result2, result3]) => {
//         const firstName = result1
//         const lastName = result2
//         const thirdName = result3
//         const name = firstName + lastName + thirdName
//         console.log(name)
//     })
// }

                                                // Menggunakan Promise cara Async Await
const getName = () => {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve('Akmal Ghaffari')
        }, 3000)
    })
}

const getAlamat = () => {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve('Tangerang')
        }, 3000)
    })
}

const getHobi = () => {
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            resolve('Music')
        }, 3000)
    })
}

const studentObject = (nama, alamat, hobi) => {
    let student = {
        nama,
        alamat,
        hobi
    }
    return student
}

const getData = async () => {
    const nama = await getName()
    const alamat = await getAlamat()
    const hobi = await getHobi()

    //Membuat object dengan memasukkan variable-variable yang sudah ditangkap dengan async await
    const data = studentObject(nama, alamat, hobi)
    
    console.log(data) 
}

getData()